	.set noreorder
	.data

	.text
	.globl main
	.ent main
main:
# int main()
# {
# 	int score = 84;
		addi $a1, $0, 84	# a1 = 84
# 	int grade;
		# a0

# 	_if (score >= 90)
		slti $t0, $a1, 90	# t0 =!(score >=90) = (score < 90)
		bne $t0, $0, L1		# if (!t0) go to L1
		nop
# 	grade = 4;
		addi $a0, $0, 4 	# a0 = 4
		j final				# go to final section of code
		nop
# 	_else if (score >= 80)
L1:
		slti $t0, $a1, 80		# t0 = !(score >=80) =(score <80)
		bne $t0, $0, L2			# if (!t0) go to L2
		nop
# 	grade = 3;
		addi $a0, $0, 3		# a0 = 3
		j final				# go to final section of code
		nop
# 	_else if (score >= 70)
L2:
		slti $t0, $a1, 70		# t0 = !(score >=80) =(score <70)
		bne $t0, $0, L3			# if (!t0) go to L3
		nop
# 	grade = 2;
		addi $a0, $0, 2 	# a0 = 2
		j final				# go to final section of code
		nop
# 	_else
L3:
#	grade = 0;
		add $a0, $0, $0		# a0 = 0
final:
		ori $v0, $0, 20 	#print a0 in hex
		syscall
		ori $v0, $0, 10      # exit
		syscall
#}
	.end main
