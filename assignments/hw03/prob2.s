	.set noreorder
	.data
#	char A[] = { 1, 25, 7, 9, -1 };
A: .byte	1, 25, 7, 9, -1

	.text
	.globl main
	.ent main

main:

#	int main()
#	{
#	int i;
		# a1
#	int current;
		# a2
#	int max;
		# a0 - > to be printed later
#	i = 0;
		add $a1, $0, $0 	# s0 = 0

#	current = A[0];
#		la $s0, A			# load address of A as s0
		lui $s0, %hi(A)
		ori $s0, $s0, %lo(A)
		lb $a2, 0($s0)		# load byte in a2
#	max = 0;
		add $a0, $0, $0		# a0 = 0
#	while (current > 0) {
loop_while:
		slt $t0, $0, $a2	# t0 = (0 < current)
		beq $t0, $0, final	# if(!t0) go to final
		nop
#	_if (current > max)
		slt $t1, $a0, $a2	# t1 = (max < current)
		beq $t1, $0, next_if	#if (!t1) go to final
		nop
#	max = current;
		add $a0, $a2, $0	# a0 = a2 + 0
next_if:
#	i = i + 1;
		addi $a1, $a1, 1		# a1 = a1 + 1
#	current = A[i];
		add $t2, $s0, $a1		# t2 = s0 + ai
		lb $a2, 0($t2)			# load byte t2 into a2
		j loop_while		#go to loop_while
		nop
#	}

final:
#	print_hex_dec(max)
		ori $v0, $0, 20 	#print max in hexadecimal
		syscall

		ori $v0, $0, 10     # exit
		syscall
	.end main
#}
