	.set noreorder
	.data

	.text
	.globl main
	.ent main
# void print(int a)
#{
Print:
#	/ should be implemented with syscall 20
		sw $ra, -12($sp)	#store ra value
		lw $a0, 36($sp)		# load input as a0
		ori $v0, $0, 20		#print a0
		syscall
		lw $ra, -12($sp)		#load ra value
		jr $ra				#jump back to previous location
		nop
#}

# int sum3(int a, int b, int c)
#{
Sum:
#		sw $ra, -8($sp)		#save ra value
		lw $t0, 20($sp)		# t0 = a
		lw $t1, 24($sp)		# t1 = b
		lw $t2, 28($sp)		# t2 = c
#	return a+b+c;
		add $t3, $t0, $t1	# t3 = t0 + t1
		add $t3, $t3, $t2	# t3 = t3 + t2
		sw $t3, 32($sp)		#save t3 as output
#		lw $ra, -8($sp)		# load ra value
		jr $ra				#jump back to previous location
		nop
#}

# int polynomial(int a, int b, int c, int d, int e)
#{
Poly:
		sw $ra, -4($sp)	#save ra value
		lw $t0, 0($sp)		#t0 = a
		lw $t1, 4($sp)		#t1 = b
		lw $t2, 8($sp)		#t2 = c
		lw $t3, 12($sp)		#t3 = d
		lw $t4, 16($sp)		#t4 = e
#	int x;
						# x = t5
#	int y;
						# y = t6
#	int z;
						# y = t7
#	x = a << b;
		sll $t5, $t0, $t1	# t5 = t0 << t1
#	y = c << d;
		sll $t6, $t2, $t3	# t6 = t2 << t3
#	z = sum3(x, y, e);
		sw $t5, 20($sp)		# 1st input = x
		sw $t6, 24($sp)		# 2nd input = y
		sw $t4, 28($sp)		# 3rd input = e
		jal Sum				#jump to sum funciton
		nop
		lw $t7, 16($sp)		#load output of sum as t7
#	print(x);
		sw $t5, 36($sp)		#store t5 as input
		jal Print			#jump to print funciton
		nop
#	print(y);
		sw $t6, 36($sp)		#store t6 as input
		jal Print			#jump to print function
		nop
#	print(z);
		sw $t7, 36($sp)		#store t7 as input
		jal Print			# jump to print function
		nop
#	return z;
		lw $t7, 40($sp)		# load t7 as output
		lw $ra, -4($sp)	#load ra value
		jr $ra
		nop
#}

main:
# int main()
#{
#	int a = 2;
		addi $s0, $0, 2		#s0 = 2 = a
#	int f = polynomial(a, 3, 4, 5, 6);
		sw $s0, 0($sp)		#store s0 as 1st input
		addi $t0, $0, 3		# t0 = 3
		sw $t0, 4($sp)		#store t0 as 2nd input
		addi $t0, $0, 4		# t0 = 4
		sw $t0, 8($sp)		#store t0 as 3rd inpt
		addi $t0, $0, 5		# t0 =5
		sw $t0, 12($sp)		#store t0 as 4th input
		addi $t0, $0, 6		# t0 =6
		sw $t0, 16($sp)		#store t0 as 5th input
		jal Poly			# go to poly funciton
		nop
		lw $a1, 40($sp)		# a1 = f
#	print(a);
		sw $s0, 36($sp)		# store s0 as input
		jal Print			#jump to print function
		nop
#	print(f);
		sw $a1, 36($sp)		# store a1 as input
		jal Print			#jump to print function
		nop

		ori $v0, $0, 10     # exit
		syscall
	.end main
# }
