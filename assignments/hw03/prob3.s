	.set noreorder
	.data
#	int A[8]
A:	.space 32
	.text
	.globl main
	.ent main
main:
#	int main()
#	{
#	int i;
							# i = a1
		la $s0, A			#load address of A into s0
#	A[0] = 0;
		add $t0, $0, $0		# t0 = 0
		sw $t0, 0($s0)		#store t0 in A[0]
#	A[1] = 1;
		addi $t0, $0, 1		# t0 = 1
		sw $t0, 4($s0)		#store t0 in A[1]
#	for (i = 2; i < 8; i++) {
		addi $a1, $0, 2		# a1 = 2
loop:
		slti $t0, $a1, 8	# t0 = (i < 8)
		beq $t0, $0, final	# if (!t0) go to final
#	A[i] = A[i-1] + A[i-2];
		addi $t1, $a1, -1	# t1 = i-1,
		addi $t2, $a1, -2	# t2 = i-2
		add $t1, $t1, $t1	# t1 = double i-1 (2x i-1)
		add $t1, $t1, $t1	# t1 = 4x i-1
		add $t2, $t2, $t2	# t2 = double i-1 (2x i-2)
		add $t2, $t2, $t2	# t2 = 4x i-2
		add $t3, $a1, $a1	# t3 = double i (2x i)
		add $t3, $t3, $t3	# t3 = 4x i
		add $t1, $s0, $t1	# t1 = location of A[i-1]
		add $t2, $s0, $t2	# t2 = location of A[i-2]
		add $t3, $s0, $t3	# t3 = location of A[i]
		lw $a2, 0($t1)		# a2 = what is located at t1 -> A[i-1]
		lw $a3, 0($t2)		# a3 = what is located at t2 -> A[i-2]
		add $a0, $a2, $a3	# a0 = a2 + a3
		sw $a0, 0($t3)		# store a0 at A[i]
		addi $a1, $a1, 1	# i++
#	PRINT_HEX_DEC(A[i]);
		ori $v0, $0, 20 	#print A[i] as hexadecimal
		syscall
		j loop				# go to loop
		nop
#	}
final:
		ori $v0, $0, 10     # exit
		syscall
#}
	.end main
