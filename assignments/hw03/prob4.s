	.set noreorder
	.data

	.text
	.globl main
	.ent main
#	typedef struct record {
#	int field0;
#	int field1;
#	} record;
main:
#	int main()
#	{
#	record *r = (record *) MALLOC(sizeof(record));
		ori $a0, $0, 8		# $a0 = 8 bytes
		ori $v0, $0, 9		# malloc 8 bytes
		syscall
#	r->field0 = 100;
		addi $t0, $0, 100	# t0 = 100
		sw $t0, 0($v0)
#	r->field1 = -1;
		addi $t0, $0, -1	# t0 = -1
		sw $t0, 4($v0)
#	EXIT;
		ori $v0, $0, 10     # exit
		syscall
#	}
	.end main
