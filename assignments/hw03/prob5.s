	.set noreorder
	.data

	.text
	.globl main
	.ent main
#typedef struct elt {
#	int value;
#	struct elt *next;
#} elt;

main:
#int main()
#{
#	elt *head;
					# *head = s0
#	elt *newelt;
					# *newelt = s1
#	newelt = (elt *) MALLOC(sizeof (elt));
		ori $a0, $0, 12		# a0 = 12bytes
		ori $v0, $0, 9		#malloc 12 bytes
		syscall
		add $s1, $v0, $0	# s1 = v0 = *newelt
#	newelt->value = 1;
		addi $t0, $0, 1		# t0 = 1
		sw $t0, 0($s1)		# newelt = 1
#	newelt->next = 0;
		addi $t0, $0, 0		#t0 = 0
		sw $t0, 4($s1)		# newelt ->next = 0
#	head = newelt;
			add $s0, $s1, $0	# s0 = s1 *head ->*newelt
#	newelt = (elt *) MALLOC(sizeof (elt));
		ori $a0, $0, 12		#a0 = 12 bytes
		ori $v0, $0, 9		#malloc 12 bytes
		syscall
		add $s1, $v0, $0	# s1 = v0 = *newelt
#	newelt->value = 2;
		addi $t0, $0, 2		# t0 = 2
		sw $t0, 0($s1)		# newelt-> value = 2
#	newelt->next = head;
		add $t0, $s0, $0	# t0 = s0
		sw $t0, 4($s1)		#  newelt ->next = head
#	head = newelt;
			add $s0, $s1, $0	# s0 = s1 *head ->*newelt
#	PRINT_HEX_DEC(head->value);
		lw $a0, 0($s0)		# a0 = s0 = head->value
		addi $v0, $0, 20	# print head->value
		syscall
#	PRINT_HEX_DEC(head->next->value);
		lw $t0, 4($s0)		# t0 = 4(s0) = head->next
		lw $t1, 0($t0)		# t2 = t0 = head->next->value
		add $a0, $t1, $0	#a0 = t1
		addi $v0, $0, 20	# print head->next->value
		syscall
#	EXIT;
#}
		ori $v0, $0, 10     # exit
		syscall
	.end main
