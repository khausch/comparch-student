	.set noreorder
	.data

	.text
	.globl main
	.ent main
# void print(int a)
#{
Print:
#	/ should be implemented with syscall 20
		ori $v0, $0, 20		#print a0
		syscall
		jr $ra				#jump back to previous location
		nop
#}

# int sum3(int a, int b, int c)
#{
Sum:
		#push ra and s3
		addi $sp, $sp, -8
		sw $ra, 4($sp)
		sw $s3, 0($sp)
#	return a+b+c;
		add $s3, $a1, $a2	# s3 = a1+a2
		add $s3, $s0, $a3	# s3 = s3+a3
		add $v1, $s3, $0	#v0 = s3
		lw $ra, 4($sp)		#load ra location
		jr $ra				#jump back to previous location
		nop
#}

# int polynomial(int a, int b, int c, int d, int e)
#{
Poly:
		# push ra, s2
		addi $sp, $sp, -8
		sw $ra, 4($sp)
		sw $s2, 0($sp)
		lw $t0, 8($sp)		#t0 = d
		lw $t1, 12($sp)		#t1 = e
#	int x;
						# x = t2
#	int y;
						# y = t3
#	int z;
						# y = t4
#	x = a << b;
		sll $t2, $a1, $a2	# t2 = a1 << a2
#	y = c << d;
		sll $t3, $a3, $t0	# t3 = a3 << t0
#	z = sum3(x, y, e);
		add $a1, $t2, $0	# a1 = t2
		add $a2, $t3, $0	# a2 = t3
		add $a3, $t1, $0	# a3 = t1
		jal Sum				#jump to sum funciton
		nop
		add $t4, $v1, $0	#v1 = return from sum
#	print(x);
		add $a0, $t2, $0
		jal Print			#jump to print funciton
		nop
#	print(y);
		add $a0, $t3, $0
		jal Print			#jump to print function
		nop
#	print(z);
		add $a0, $t4, $0
		jal Print			# jump to print function
		nop
#	return z;
		lw $ra, 12($sp)		#load ra value
		add $v0, $t4, $0	#v0 = t4
		jr $ra
		nop
#}

main:
# int main()
#{
# Push ra, s0, s1, s2, space for d and e
		addi $sp, $sp, -24
		sw $ra, 20($sp)
		sw $s0, 16($sp)
		sw $s1, 12($sp)
		sw $s2, 8($sp)
#	int a = 2;
		addi $s0, $0, 2		#s0 = 2 = a
#	int f = polynomial(a, 3, 4, 5, 6);
		addi $a1, $s0, 0	# a1 = a = s0
		addi $a2, $0, 3		# a2 = 3
		addi $a3, $0, 4		# a3 =4
		addi $t0, $0, 5		# t0 = 5
		sw $t0, 0($sp)		#store t0 as 5th input
		addi $t0, $0, 6		# t0 =6
		sw $t0, 4($sp)		#store t0 as 6th input
		jal Poly			# go to poly funciton
		nop
		add $s1, $v0, $0	#v0 = return of Poly
#	print(a);
		add $a0, $s0, $0
		jal Print			#jump to print function
		nop
#	print(f);
		add $a0, $s1, $0
		jal Print			#jump to print function
		nop
End:
		ori $v0, $0, 10     # exit
		syscall
	.end main
# }
